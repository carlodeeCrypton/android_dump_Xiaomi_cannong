#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_cannong.mk

COMMON_LUNCH_CHOICES := \
    lineage_cannong-user \
    lineage_cannong-userdebug \
    lineage_cannong-eng
